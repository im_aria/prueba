
package pruebasa;

import java.util.Scanner;

public class Pruebasa {

    
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        System.out.println("ESCRIBE EL NUMERO DE FILAS");
        int nFilas =Integer.parseInt(sc.nextLine());
        System.out.println("ESCRIBE EL NUMERO DE COLUMNAS");
        int nColumnas =Integer.parseInt(sc.nextLine());
        //Pedir por teclado el numero de filas y el numero de columnas
        
        
        int[][] matriz=new int[nFilas][nColumnas];
        //LLamar a las 3 funciones e impriman el resultado las 3.
        
        rellenar(matriz);
        System.out.println("");
        rellenarMul3(matriz);                  
        System.out.println("");
        rellenarAlReves(matriz);
        
    }
    public static void rellenar(int [][] matriz){
        //Rellene la matriz con numeros consecutivos.Por ejemplo:
        //Si la matriz es de 3x4
        //0 1 2 3
        //4 5 6 7
        //8 9 10 11
        

        for(int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
                
                matriz[i][j]=j+i*matriz[i].length;
                System.out.print(matriz[i][j]+" "); 
                
            }
            System.out.println("");
            
            
        }
        
        
        
    }

    public static void rellenarMul3(int [][] matriz){
    //Rellene la matriz con múltiplos de 3. Ejemplo:
    //Matriz 3x4
    //0 3 6 9
    //12 15 18 21
    //24 27 30 33
    
     for (int i = 0; i < matriz.length; i++){
                for (int j = 0; j < matriz[i].length; j++){
                    matriz[i][j]=(j+i*matriz[i].length)*3;
                    
                    System.out.print(matriz[i][j]+" ");
                    }
                System.out.println(" ");

    }
    }
    public static void rellenarAlReves(int [][] matriz){
    //Rellene la matriz de forma inversa. Ejemplo:
    //Matriz 3x4
    //11 10 9 8
    //7 6 5 4
    //3 2 1 0
    
    

        for(int i=matriz.length-1;i>=0;i--){
            for(int j=matriz[i].length-1;j>=0;j--){
                
                matriz[i][j]=((j+i*matriz[i].length)*-1);
                System.out.print(matriz[i][j]+" ");             
                
                
            }
            System.out.println("");
            
            
        }
    
    }
}
